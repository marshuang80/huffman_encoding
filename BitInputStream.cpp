#include <iostream>
#include "BitInputStream.hpp"


    void BitInputStream::fill(){
        rounds++;
        buf = in.get(); 
	nbits = 0;
    }

    int BitInputStream::readBit(){
	if((nbits == 8 || first == true)) fill(); 
        first = false; 
        int num = (buf >> nbits)&1;
        nbits++; 
      return num;
    }
    bool BitInputStream::eof() {
	if (in.eof()){
             return true;
	}
        return false;
    } 

    void BitInputStream::setTotal(int total){
        tbits = total; 
    }
    int BitInputStream::getTotal() const{
	return tbits;
    }
    int BitInputStream::getRounds() const{
	return rounds;
    } 
    int BitInputStream::getNbits() const{
	return nbits;
    }
