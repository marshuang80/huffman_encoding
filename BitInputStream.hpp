#ifndef BITINPUTSTREAM_HPP
#define BITINPUTSTREAM_HPP

#include <fstream>
#include <iostream>
using namespace std;

class BitInputStream {

private:
    bool first;
    bool lastround; 
    char buf;
    int nbits;
    int tbits;
    int rounds;
    int i =  0;  
    std::istream &in;
public:
    BitInputStream(std::istream &is):in (is) {
	buf = nbits = 0;
        first = true; 
        lastround = false;
	rounds = 0; 
    } 
//int rounds; 
//    int tbits; 
  void fill();
    int readBit();
    bool eof();
    void setTotal(int total);
    int getTotal() const;
    int getRounds() const;
    int getNbits() const; 
};

#endif
