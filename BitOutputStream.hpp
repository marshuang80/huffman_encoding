#ifndef BITOUTPUTSTREAM_HPP
#define BITOUTPUTSTREAM_HPP

#include <iostream>
using namespace std;
class BitOutputStream {

private:
    char buf;
    int nbits;
    ostream & out;
public:
    BitOutputStream(std::ostream &os) : out(os), buf(0), nbits(0) { }

    void flush();
    void writeBit(int i);

};

#endif
