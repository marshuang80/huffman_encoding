/*
 * Rohan Paul and Shih-Cheng Huang
 *
 * A99056989 and A99079114
 *
 */

#include <iostream>
#include <vector> 
#include <fstream>
#include "HCNode.hpp" 
#include "HCTree.hpp" 
#include "BitOutputStream.hpp"

using namespace std; 

int main(int argc, char* argv[]) {
    
    if (argc!= 3) {
	cout << "Correct usage: ./compress infile outfile" << endl;
        return -1;
    }
    
    vector<int> freqs(256,0);
    ifstream infile;
    unsigned char nextChar;
    int nextByte;
    infile.open(argv[1], ios::binary);
//    ifstream infile(argv[1]);
    if (infile.is_open()) {
        while((nextByte = infile.get()) != EOF) {
	    nextChar = (unsigned char) nextByte;
	    freqs[nextChar]++;
        }
    infile.close();
    }
    HCTree tree;
    tree.build(freqs);


//for (int a: freqs) { cout << a << " ";}
//cout <<endl;
    ofstream outfile;
    outfile.open(argv[2]);
    BitOutputStream bout(outfile);

//print header into file

    for (int i = 0; i < freqs.size(); i++) {
 //  	  outfile << freqs[i] << " "; 
        // to store max of 10M
        outfile.write((char*)&freqs[i],3);
    }
//    outfile << endl; 

//encode
    ifstream infile1(argv[1], ios::binary);
    if (infile1.is_open()){
	byte c; 
	while(1){
            if(infile1.eof()){
                bout.flush();
                break;
            }
/*	    if(infile1.get() == EOF){
		bout.flush();
		break;
	    }
*/
	    infile1.read((char*)&c, sizeof(c));  
        if(!infile1.eof()) tree.encode(c, bout);
    
    }	
    infile1.close();
    outfile.close();

}




//build frequency vector   
/*
    HCTree tree; 
    vector<int> freqs(256,0); 
    ifstream infile(argv[1]);
    if (infile.is_open()) {
	while(1){
	    int character = infile.get();
    if (infile.eof()) break;
	    freqs[character]+=1;
	}
    tree.build(freqs);
   }
infile.close();


//print header into file
    ofstream outfile(argv[2]);

    for (int i = 0; i < freqs.size(); i++) {
	outfile << freqs[i] << " "; 
    }
    outfile << endl;

//encode
    ifstream infile1(argv[1]);
    if (infile1.is_open()){
	unsigned int c; 
	while(1){
		if(infile1.eof()) break;
		c = infile1.get();  
		if(c < 256){
		        tree.encode(c, outfile);
		}
}	
    infile1.close();
    outfile.close();
}
*/ 
}
