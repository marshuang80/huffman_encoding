/*
 * Rohan Paul and Shih-Cheng Huang
 *
 * A99056989 and A99079114
 *
 */

#include <queue>
#include <vector>
#include <fstream>
#include "HCTree.hpp"
#include <string> 
#include "BitOutputStream.hpp"
#include "BitInputStream.hpp"
using namespace std;

HCTree::~HCTree(){
    deleteall(root);    
}

void HCTree::build(const vector<int>& freqs) {
 
    priority_queue<HCNode*, vector<HCNode*>, HCNodePtrComp> queue;
    for (int i = 0; i < freqs.size(); i++) {
	if(freqs[i] != 0){
	    HCNode* node = new HCNode(freqs[i],i);
	    queue.push(node);
	    leaves[i] = node;
	}
    }

    while(queue.size() > 1 ){
	HCNode* first = queue.top();
        queue.pop();
        HCNode* second = queue.top(); 
        queue.pop(); 
        int frequency = first->count + second->count; 
        byte symbol = first->symbol ; 
        HCNode* parent = new HCNode(frequency, symbol, first, second);
        root = parent; 
	parent->c0->p = parent ;
        parent->c1->p = parent;
        queue.push(parent);
    }
}

/*
 *
 * Commented out for the checkpoint
 */

void HCTree::encode(byte symbol, BitOutputStream& out)const { 
    HCNode* child = leaves[symbol];
    string temp; 
    while(child-> p){
	if(child == child-> p -> c0){
//	    out.writeBit(0);
            temp += "0";
//            total++; 
    	}
	else{
//	    out.writeBit(1);
            temp += "1";
//            total++; 
	}
	child = child -> p; 
    }
    for (int i = temp.size()-1; i >=0; i --){ 
	int input;
	if(temp[i] == '0') input = 0;
	else input = 1;
	out.writeBit(input);
    }

}

void HCTree::encode(byte symbol, ofstream& out) const {
    string code = "";
    HCNode* child = leaves[symbol];
    while(child-> p){
	if(child == child-> p -> c0){
	    code += "0";
	}
	else{
	    code += "1";
	}
	child = child -> p; 
    }
    for (int i = code.size() -1; i >= 0; i--) {
	out << code[i];         
    }
}
/*
 *Commented out for the checkpoint
 *
 **/
int HCTree::decode(BitInputStream& in) const {
    int symbol;
    unsigned int num; 
    HCNode* curr = root;
    while(1){
        num = in.readBit();  
	if(num == 0 && curr -> c0 != nullptr){
	    curr = curr -> c0; 
	}
	else if (num == 1 && curr -> c1 != nullptr){
	    curr = curr -> c1;
	}		    
	if (curr-> c1 == nullptr && curr -> c0 == nullptr){
	    symbol = curr -> symbol; 
	    break; 
	}
    }
    return symbol; 

}
int HCTree::decode(ifstream& in ) const{
    int symbol;
    unsigned char num; 
    HCNode* curr = root; 
    while(1){
	num = in.get(); 
	if(in.eof()) return -1; 
	if(num == '0' && curr -> c0 != nullptr){
	    curr = curr -> c0; 
	}
	else if (num == '1' && curr -> c1 != nullptr){
	    curr = curr -> c1;
	}		    
	if (curr-> c1 == nullptr && curr -> c0 == nullptr){
	    symbol = curr -> symbol; 
	    break; 
	}
    }
    return symbol; 
}

int HCTree::totalBits()const{
    int tbit = 0;
    for (int i = 0; i < leaves.size(); i++) {    
        int codelength = 0; 
	if(leaves[i] != nullptr){
            HCNode* curr = leaves[i];
            while(curr -> p){
                curr = curr ->p; 
                codelength ++; 
            }
            codelength = codelength * (leaves[i] -> count); 
        }
        tbit = tbit + codelength;
    }
    return tbit; 
}

void HCTree::deleteall(HCNode* root) {
    if (root == nullptr) {
        return;       
    }
    if (root->c0 != nullptr) {
        deleteall(root->c0);    
    }
    if (root->c1 != nullptr){
        deleteall(root->c1);    
    } 
    delete(root);    
}




int HCTree::totalChar()const{
    int tchar = 0;
    for (int i = 0; i < leaves.size(); i++) {    
	if(leaves[i] != nullptr){
            tchar += (leaves[i] -> count); 
	}
    }
    return tchar; 
}
