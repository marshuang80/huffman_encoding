/*
 * Rohan Paul and Shih-Cheng Huang
 *
 * A99056989 and A99079114
 *
 */
#include <sstream>
#include <vector> 
#include <fstream>
#include "HCNode.hpp" 
#include "HCTree.hpp" 
#include <string>
using namespace std; 

int main(int argc, char* argv[]) {
/*
    if (argc != 3) {
	cout << "Correct usage: ./uncompress infile outfile" << endl;
	return -1;
    }
    HCTree tree; 
    ifstream infile(argv[1]);
    vector<int> header(256,0); 
    string head = "";
    getline(infile, head);
    string num;
    for(int i = 0, j = 0; i < head.size(); i++){
	if(head[i] != ' ') num += head[i]; 
	else{
	    int value = atoi(num.c_str());
	    header[j] = value;
	    j++;  
	    num = "";
	}
    }
    tree.build(header); 
    ofstream outfile(argv[2]); 
    unsigned int input; 
    BitInputStream bin(infile); 
    while (1){
	input = tree.decode(infile); 
	if(infile.eof()) break; 
	outfile <<  (char)input; 
    }
    
    infile.close();
    outfile.close();

  
*/    if (argc != 3) {
	cout << "Correct usage: ./uncompress infile outfile" << endl;
	return -1;
    }
    HCTree tree; 
    ifstream infile(argv[1], ios::binary);
    vector<int> header(256,0); 
    for(int i = 0; i < header.size(); i++){
        int input = 0;
        infile.read((char*)&input, 3); 
        header[i] = input;
    }
 /*   string head = "";
      getline(infile, head);
      string num;
      for(int i = 0, j = 0; i < head.size(); i++){
	if(head[i] != ' ') num += head[i]; 
	else{
	    int value = atoi(num.c_str());
	    header[j] = value;
	    j++;  
	    num = "";
	}
    }
*/
    tree.build(header); 
    BitInputStream bin(infile); 
    bin.setTotal(tree.totalBits());
    ofstream outfile(argv[2]); 
    unsigned int input; 
int numchar = 0;     
    while (1){
if(numchar == tree.totalChar()) break; 
	input = tree.decode(bin);
	outfile <<  (char)input;
	numchar ++;  
    }
    
    infile.close();
    outfile.close();
}
